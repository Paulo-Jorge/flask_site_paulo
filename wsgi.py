#!/usr/bin/env python
# -*- coding: utf-8 -*-

from index import app
#from index import app as application

# For debugging; will not run if launched from Nginx
if __name__ == "__main__":
    #app.debug = True
    #retirar o threaded=True, só incluí porque o server de teste fechava coneções de tempos a tempos e isto parece ajudar
    #app.run(threaded=True)
    #application.run(port=8080, debug=True, host="0.0.0.0")
    app.run(port=8080, debug=True, host="0.0.0.0")
