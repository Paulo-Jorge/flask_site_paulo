# -*- coding: utf-8 -*-

import os
from flask import Flask, render_template, request, redirect, url_for, session
#from flask import Flask, request
from flask_sqlalchemy import SQLAlchemy
import flask_migrate
from flask_admin import Admin, form, BaseView, expose
from flask_admin.form import rules
from flask_admin.contrib.sqla import ModelView
#from flask_mail import Mail
from flask_recaptcha import ReCaptcha
from datetime import datetime

from flask_cors import CORS

#For use Mako instead of Jinja2
#from flask.ext.mako import MakoTemplates, render_template

#Start the basic stuff: App, DB ORM/Migrations and Email
app = Flask(__name__)
CORS(app)
#For use Mako instead of Jinja2
#mako = MakoTemplates(app)
app.config.from_object('config')
db = SQLAlchemy(app)
migrate = flask_migrate.Migrate(app, db)
#mail = Mail(app)
recaptcha = ReCaptcha(app=app)

# Create directory for photos db to upload
photos_path = os.path.join(os.path.dirname(__file__), 'static', 'photos')
#try:
#    os.mkdir(photos_path)
#except OSError:
#    pass

# Create directory for musics db to upload
musics_path = os.path.join(os.path.dirname(__file__), 'static', 'music')
#try:
#    os.mkdir(musics_path)
#except OSError:
#    pass

#Define Models
post_tags_table = db.Table('post_tags', db.Model.metadata,
                           db.Column('post_id', db.Integer, db.ForeignKey('posts.id')),
                           db.Column('tag_id', db.Integer, db.ForeignKey('tags.id'))
                           )

photos_tags_table = db.Table('photos_tags', db.Model.metadata,
                           db.Column('photos_id', db.Integer, db.ForeignKey('photos.id')),
                           db.Column('photostags_id', db.Integer, db.ForeignKey('photostags.id'))
                           )

class Posts(db.Model):
    __tablename__ = 'posts'

    id = db.Column(db.Integer, primary_key = True)
    title = db.Column(db.String(255))
    title_en = db.Column(db.String(255))
    body = db.Column(db.Text)
    body_en = db.Column(db.Text)
    #body = db.Column(db.UnicodeText) ???
    #tags = db.Column(db.String(255), db.ForeignKey("tags.id"))
    #tags_id = db.Column(db.Integer, db.ForeignKey("tags.id"))
    #tags = db.relationship('Tags', backref='posts', lazy='dynamic')

    tags = db.relationship('Tags', secondary=post_tags_table, backref='posts')

    #active = db.Column(db.Boolean)
    on_off = db.Column(db.Boolean, default=True)
    #Created this type Column just because in the future I am thinking of having different types of posts. But not needed for now.
    type = db.Column(db.String(255), default="blog")
    author = db.Column(db.String(255), default="Paulo Jorge PM")
    pub_date = db.Column(db.DateTime, default=datetime.now())
    #pub_date = db.Column(db.DateTime, default=datetime.now())
    #pub_date = db.Column(db.DateTime, default=datetime.utcnow())
    extra_field1 = db.Column(db.String(255))
    def __repr__(self):
        return '<Post %r>' % (self.body)
        
class Rascunhos(db.Model):
    __tablename__ = 'rascunhos'

    id = db.Column(db.Integer, primary_key = True)
    body = db.Column(db.Text)
    type = db.Column(db.String(255), default="blog")
    author = db.Column(db.String(255), default="Paulo Jorge PM")
    pub_date = db.Column(db.DateTime, default=datetime.now())
    def __repr__(self):
        return '<Rascunho %r>' % (self.body)

class Tags(db.Model):
    __tablename__ = 'tags'

    id = db.Column(db.Integer, primary_key = True)
    tag = db.Column(db.Unicode(64))
    tag_en = db.Column(db.Unicode(64))

    def __str__(self):
        return self.tag

class Musics(db.Model):
    __tablename__ = 'musics'

    id = db.Column(db.Integer, primary_key = True)
    title = db.Column(db.String(255))
    path = db.Column(db.Unicode(255))
    day_start = db.Column(db.Date, default=datetime.now())
    youtube_link = db.Column(db.String(255))
    week_number = db.Column(db.Integer)
    on_off = db.Column(db.Boolean, default=False)
    extra_field1 = db.Column(db.String(255))

    def __unicode__(self):
        return self.title

class Photos(db.Model):
    #__tablename__ = 'photos'

    id = db.Column(db.Integer, primary_key = True)
    title = db.Column(db.String(255))
    title_en = db.Column(db.String(255))
    album = db.Column(db.String(255), default="geral")
    #personal_albums = db.relationship('PersonalAlbums', secondary=personal_albums_table, backref='photos')
    path = db.Column(db.Unicode(255))
    tags = db.relationship('PhotosTags', secondary=photos_tags_table, backref='photos')
    pub_date = db.Column(db.DateTime, default=datetime.now())
    on_off = db.Column(db.Boolean, default=True)
    #was thinking some extra stuff, but no time. This field is for that in the future. Ex.: link for a post with info/raw files about the photo etc
    extra_field1 = db.Column(db.String(255))

    def __unicode__(self):
        return self.title

class PhotosTags(db.Model):
    __tablename__ = 'photostags'

    id = db.Column(db.Integer, primary_key = True)
    tag = db.Column(db.Unicode(64))

    def __str__(self):
        return self.tag

def is_number(x): #determina se na string do arg existe um número
    try:
        float(x)
        return True
    except:
        return False

"""def is_letters_digits(x): #determina se contém apenas letras e números. Filtra todo o tipo de símbolos e palavras acentuadas.
    if re.match("^[0-9 a-z A-Z]+$", x):
        return True
    else:
        return False"""

#data available to all temples - f.e. music for the layout top bar
@app.context_processor
def global_data():
    lang = request.args.get("lang", default="pt", type=None)
    if lang not in ["pt", "en"]:
        lang = "pt"
    musics = Musics.query.filter(Musics.on_off == True).order_by("id")
    return dict(musics=musics, lang=lang)

#Data for the right template - it has not router, so we call from the functions
def right_index():
    last_photos = Photos.query.with_entities(Photos.path).filter((Photos.id >= 0) & (Photos.on_off == True) ).order_by("id desc").limit(1)
    recent_posts = Posts.query.with_entities(Posts.title, Posts.id).filter( (Posts.id >= 0) & (Posts.on_off == True) ).order_by("id desc").limit(7)
    return dict(last_photos=last_photos, recent_posts=recent_posts)

@app.route("/index")
@app.route("/")
def index():
    right = right_index()
    return render_template("index.html", right=right)

@app.route("/blog")
@app.route("/blog/<int:page>")
@app.route("/blog/<int:page>", methods=['POST'])
@app.route("/blog", methods=['POST'])
def blog(page=None):

    if request.method == 'POST':
        return redirect( url_for('blog', page=int(request.form['post_id'])) )

    NAV_AFTER_BEFORE_EXTRA = 2
    total_posts = Posts.query.filter(Posts.id > 0).count()

    if page and page <= total_posts:
        p = page
    else:
        #p = 1
        #p = total_posts = Posts.query.filter(Posts.id > 0).count()
        p = total_posts
    #posts = Posts.query.filter(Posts.id == p).first()
    post = Posts.query.filter(Posts.id == p).first_or_404()
    #for post in posts:
        #post.tags = re.split('[\W]*', unicode(post.tags,'utf-8'), flags=re.UNICODE)
        #post.tags = re.split('[\W]*', unicode(post.tags,'utf-8'), flags=re.UNICODE)


    #nav_before ="1"
    #total_posts="2"
    #nav_before="3"
    #next_page="4"
    #previous_page="5"
    #nav_after="6"

    #n_navs = total_posts % POSTS_PER_PAGE

    #lowest to highest
    #nav_posts = Posts.query.filter( (Posts.id >= p) & (Posts.id < p + POSTS_PER_PAGE) )

    #if normal situation
    #if p >= POSTS_PER_PAGE:
    if p > NAV_AFTER_BEFORE_EXTRA:
        #if we are at the limit of the maximum id do this, else do normal: p.e. if NAV_AFTER_BEFORE_EXTRA=2: 2 latest posts + 2 most recent posts + current post in middle
        if p > total_posts - NAV_AFTER_BEFORE_EXTRA:
            nav_posts = Posts.query.filter( (Posts.id <= total_posts) & (Posts.id > total_posts - (NAV_AFTER_BEFORE_EXTRA * 2 + 1)) ).order_by("id desc")
        else:
            #nav_posts = Posts.query.filter( (Posts.id <= p) & (Posts.id > p - POSTS_PER_PAGE) ).order_by("id desc")
            #if p
            #nav_posts = Posts.query.filter( (Posts.id <= p + 2) & (Posts.id > p - 2) ).order_by("id desc")
            nav_posts = Posts.query.filter( (Posts.id <= p + NAV_AFTER_BEFORE_EXTRA) & (Posts.id >= p - NAV_AFTER_BEFORE_EXTRA) ).order_by("id desc")
    #else if it is the last posts and there are less than what we want per page
    else:
        # NAV_AFTER_BEFORE_EXTRA * 2 + 1 = total of nav posts: f.e. if NAV_AFTER_BEFORE_EXTRA=2: 2 up, 2 down, + the middle one
        nav_posts = Posts.query.filter(Posts.id <= NAV_AFTER_BEFORE_EXTRA * 2 + 1).order_by("id desc")

    #nav_posts = Posts.query.filter( and_(Posts.id > p, Posts.id < p + POSTS_PER_PAGE) )

    #nav_before="1"
    #nav_after="2"

    """
    #inc
    import math
    #math.ceil = round always up. e.g.: 2.1 = 3
    total_navs = math.ceil(total_posts / POSTS_PER_PAGE)
    nav = Posts.query.filter(and_(Posts.id > page, Posts.id < page + POSTS_BEFORE_AFTER)).all
    """

    """
    #Nav Items: show present page in middle, and POSTS_BEFORE_AFTER nº last before and POSTS_BEFORE_AFTER nº next after. If page is in the start or end do different
    if page > POSTS_BEFORE_AFTER:
        nav_before = Posts.query.filter(and_(Posts.id > page - POSTS_BEFORE_AFTER, Posts.id < page)).all
        #if page < total_posts - POSTS_BEFORE_AFTER:
        #    nav_after = Posts.query.filter(and_(Posts.id > page, Posts.id < page + POSTS_BEFORE_AFTER)).all
        #else:
        #    nav_after = Posts.query.filter(and_(Posts.id > page)).all
    else:
        nav_before = Posts.query.filter(Posts.id < page).all

    if page < total_posts - POSTS_BEFORE_AFTER:
        nav_after = Posts.query.filter(and_(Posts.id > page, Posts.id < page + POSTS_BEFORE_AFTER)).all
    else:
        nav_after = Posts.query.filter(Posts.id > page).all

    """

    #pagination = Posts.query.paginate(page, POSTS_PER_PAGE, False)
    if p > 1:
        previous_page = str(p -1)
    else:
        previous_page = 1
    if p < int(total_posts):
        next_page = str(p + 1)
    else:
        next_page = total_posts
    return render_template("blog.html", post=post, page=p, previous_page=previous_page, next_page=next_page, total_posts=total_posts, nav_posts=nav_posts)

@app.route("/portfolio")
def portfolio():
    return render_template("portfolio.html")

@app.route("/rss.atom")
def rss():
    from werkzeug.contrib.atom import AtomFeed

    feed = AtomFeed('Recent blog posts',
                    feed_url=request.url, url=request.url_root)
    articles = Posts.query.order_by(Posts.pub_date.desc()) \
                      .limit(15).all()
    for article in articles:
        feed.add(article.title, article.body,
                 content_type='html',
                 author="Paulo Jorge PM",
                 url=request.url_root + "blog/"+str(article.id),
                 updated=article.pub_date)
                # published="yes")
    response = feed.get_response()
    #response.headers["Content-Type"] = "application/atom+xml"
    return response

#return render_template("sucess.html", right=right)

@app.route("/sucess")
def sucess():
    right = right_index()
    return render_template("sucess.html", right=right)

@app.route("/failed")
def failed():
    right = right_index()
    txt=session['txt']
    mail=session['mail']
    return render_template("failed.html", right=right, txt=txt, mail=mail)

@app.route("/send_mail_cv", methods=['POST'])
def send_mail_cv():
    if request.method == 'POST':
        if recaptcha.verify():
            #Save in a log file
            body = request.form['info'].encode(encoding='UTF-8')
            email = request.form['email'].encode(encoding='UTF-8')
            date_now = datetime.now().strftime("%Y/%m/%d %H:%M:%S")
            if request.form['type'] == "cv":
                txt_emails = os.path.join(os.path.dirname(__file__), 'logs', 'emails_cv.txt')
            elif request.form['type'] == "shop":
                txt_emails = os.path.join(os.path.dirname(__file__), 'logs', 'emails_shop.txt')
            else:
                txt_emails = "Erro"
            with open(txt_emails, "a") as txtfile:
                txtfile.write("<email>\r\n"+ date_now + "\r\nDe: " + email + "\r\nE-mail: \"" + body + "\"\r\n</email>\r\n")

            """from flask_mail import Message
            msg = Message("subject a ver", sender = "paulo.jorge.pm@ilch.uminho.pt", recipients=["paulo.jorge.pm@gmail.com"])
            msg.body = "teste teste"
            msg.html = "tste hml"
            mail.send(msg)"""

            """import smtplib
            server = smtplib.SMTP('mail.uminho.pt', 25)
            server.starttls()
            server.login("xxxx", "xxxxxx")
            msg = "mail teste"
            server.sendmail("paulo.jorge.pm@ilch.uminho.pt", "paulo.jorge.pm@gmail.com", msg)
            server.quit()"""

            # Parts of an email
            import smtplib
            from email.mime.text import MIMEText
            SERVER = app.config['MAIL_SERVER']
            PORT = app.config['MAIL_PORT']
            USER = app.config['MAIL_USERNAME']
            PASS = app.config['MAIL_PASSWORD']
            FROM = app.config['MAIL_DEFAULT_SENDER']
            TO = ['paulo.jorge.pm@gmail.com', 'paulo.jorge.pm@ilch.uminho.pt']
            SUBJECT = 'PAULO JORGE PM: MAIL ' + request.form['type']

            # Create the email
            message = MIMEText(date_now + "\r\nDe: " + email + "\r\nE-mail: " + body)
            message['From'] = FROM
            message['To'] = ",".join(TO)
            message['Subject'] = SUBJECT

            # Sends an email
            email = smtplib.SMTP()
            email.connect(SERVER,PORT)
            email.starttls()
            email.login(USER,PASS)
            email.sendmail(FROM, TO, message.as_string())
            email.quit()

            return redirect( url_for('sucess'))
        else:
            session["txt"]=request.form['info'].encode(encoding='UTF-8')
            session["mail"]=request.form['email'].encode(encoding='UTF-8')
            return redirect( url_for('failed') )

@app.route("/tags/<int:page>")
@app.route("/tags/<int:page>", methods=['POST'])
@app.route("/tags", methods=['POST'])
@app.route("/tags")
def tags(page=1):
    SHOW_POSTS = 10
    tag = request.args.get("tag", default=None, type=None)

    if request.method == 'POST':
        return redirect( url_for('tags', page=int(request.form['post_id'])) + "?tag=" + tag )

    import math
    if tag:
        keyword = Tags.query.filter(Tags.tag == tag).first()
        if keyword:
            posts_query = Posts.query.filter(Posts.tags.contains(keyword))
            total = posts_query.count()
            if page and page*SHOW_POSTS <= total:
                page = page
            else:
                page = int(math.ceil(float(total)/SHOW_POSTS))
            posts = Posts.query.filter(Posts.tags.contains(keyword)).paginate(page, SHOW_POSTS, True)

        else:
            posts = ""
    else:
        posts = ""

    if page > 1:
        previous_page = str(page - 1)
    else:
        previous_page = 1
    if page < math.ceil(float(total)/SHOW_POSTS):
        next_page = str(page + 1)
    else:
        next_page = math.ceil(float(total)/SHOW_POSTS)
    last_page = int(math.ceil(float(total)/SHOW_POSTS))

    tags = Tags.query.filter(Tags.id > 0).order_by("id desc")
    return render_template("tags.html", tag=tag, posts=posts, tags=tags, previous_page=previous_page, last_page=last_page, next_page=next_page)

@app.route("/photos")
@app.route("/photos/<int:page>")
@app.route("/photos/<int:page>", methods=['POST'])
@app.route("/photos", methods=['POST'])
#def photos(filter=None):
def photos(page=1):

    #photos_dir = "/static/photos/thematic"

    #import photos_albums
    #last_photo = photos_albums.last_photo

    #albums = photos_albums.photos_thematic

    #base_dir = os.path.dirname(__file__)
    #dir = os.path.join(base_dir, "static","photos","thematic")
    #list = os.listdir(dir)
    #list = []
    #for item in os.listdir(dir):
    #    if os.path.isdir(os.path.join(dir, item):


    #list = [os.path.join(dir, o) for o in os.listdir(dir) if os.path.isdir(os.path.join(dir,o))]


    #Show JPG metadata
    #import exifread
    # Open image file for reading (binary mode)
    #photo = open(os.path.join(base_dir, "static","photos","thematic",list[0]), 'rb')
    # Return Exif tags
    #tags = exifread.process_file(photo, details=False)

    tags = None
    SHOW_PHOTOS = 16
    #total_photos = Photos.query.filter(Photos.id > 0).count()
    total_photos = 0
    photos_query = None
    tag = request.args.get("tag", default=None, type=None)
    album = request.args.get("filter", default="geral", type=None)

    if request.method == 'POST':
        args=""
        if tag:
            args="?tag=" + tag
        elif album != "geral":
            args="?filter=" + album
        return redirect( url_for('photos', page=int(request.form['post_id'])) + args )

    #Note: if there is no photos with the personal or conceptual album name, it will give error if choosed!
    #SInce the website is mine I don't care, but foreigners: populate the db 1st!
    if album == "personal":
        photos_query = Photos.query.filter(Photos.album == "personal").order_by("id desc")
    elif album == "conceptual":
        photos_query = Photos.query.filter(Photos.album == "conceptual").order_by("id desc")
    #if normal photo view
    else:
        if tag:
            keyword = PhotosTags.query.filter(PhotosTags.tag == tag).first()
            if keyword:
                photos_query = Photos.query.filter(Photos.tags.contains(keyword)).order_by("id desc")
            else:
                return redirect( url_for('photos', page=1) )
        #show all
        else:
            photos_query = Photos.query.filter(Photos.id > 0).order_by("id desc")
        tags = PhotosTags.query.filter(PhotosTags.id > 0).order_by("id desc")

    total_photos = photos_query.count()

    import math
    if page and page*SHOW_PHOTOS <= total_photos:
        page = page
    else:
        page = int(math.ceil(float(total_photos)/SHOW_PHOTOS))

    if page > 1:
        previous_page = str(page - 1)
    else:
        previous_page = 1
    if page < math.ceil(float(total_photos)/SHOW_PHOTOS):
        next_page = str(page + 1)
    else:
        next_page = math.ceil(float(total_photos)/SHOW_PHOTOS)
    last_page = int(math.ceil(float(total_photos)/SHOW_PHOTOS))
    #Note: the paginate func of the SQLAlchemy have atributes for some of what I made above: it has next page, previous, etc. already. But I prefered to do it by hand, more fun and flexible,but extra work too

    photos = photos_query.paginate(page, SHOW_PHOTOS, True)


    """
    if album == "personal":
        photos = Photos.query.filter(Photos.album == "personal").order_by("id desc").paginate(page, SHOW_PHOTOS, True)
    elif album == "conceptual":
        photos = Photos.query.filter(Photos.album == "personal").order_by("id desc").paginate(page, SHOW_PHOTOS, True)
    #show all
    else:
        if tag:
            #photos = Photos.query.filter(Photos.tags.contains("novo")  ).order_by("id desc").paginate(page, SHOW_PHOTOS, True)
            keyword = PhotosTags.query.filter(PhotosTags.tag == tag).first()
            if keyword:
                photos_query = Photos.query.filter(Photos.tags.contains(keyword))
                photos = photos_query.paginate(page, SHOW_PHOTOS, True)
                total_photos = photos_query.count()
            else:
                photos = Photos.query.filter(Photos.id > 0).order_by("id desc").paginate(page, SHOW_PHOTOS, True)
            #photos = Photos.query.join(Photos.tags).filter(Photos.tags.contains("novo")  ).order_by("id desc").paginate(page, SHOW_PHOTOS, True)
        else:
            photos = Photos.query.filter(Photos.id > 0).order_by("id desc").paginate(page, SHOW_PHOTOS, True)
        tags = PhotosTags.query.filter(PhotosTags.id > 0).order_by("id desc")


    #photos = Photos.query.filter(Photos.id >= 0).order_by("id desc")

    #if filter:
    #album = request.args.get("filter", default="all", type=None)
    #if request.args['filter']:
        #album = filter
    #    album = "request.args['filter']"
    #else:
    #    album = "all"
    """
    #return render_template("photos.html", dir=dir, list=list, tags=tags, os=os, last_photo=last_photo, albums=albums)
    return render_template("photos.html", photos=photos, album=album, next_page=next_page, previous_page=previous_page, total_photos=total_photos, last_page=last_page, tags=tags, tag=tag)

@app.route("/photo_viewer/<int:photo_id>")
def photo_viewer(photo_id=None):

    iframe = request.args.get("iframe", default=None, type=None)

    photo = Photos.query.filter(Photos.id == photo_id).first_or_404()
    data=""
    #JPG metadata
    import exifread
    #Open image file for reading (binary mode)
    photo_file = open(os.path.join(photos_path,photo.path), 'rb')
    #Return Exif tags
    data = exifread.process_file(photo_file, details=False)
    return render_template("photo_viewer.html", photo=photo, data=data, iframe=iframe)

@app.route("/shop")
def shop():
    return render_template("shop.html")

@app.route("/cv")
def cv():
    right = right_index()
    #db.create_all()
    return render_template("cv.html", right=right)

@app.route("/contact")
@app.route("/contact", methods=['POST'])
def contact():
    right = right_index()
    mail = ""

    if request.method == 'POST' and recaptcha.verify():
        #Save in a log file
        email = request.form['email'].encode(encoding='UTF-8')
        date_now = datetime.now().strftime("%Y/%m/%d %H:%M:%S")
        txt_emails = os.path.join(os.path.dirname(__file__), 'logs', 'newsletter_email.txt')
        with open(txt_emails, "a") as txtfile:
            txtfile.write(email + " - Data: " + date_now + "\r\n")
        #SEND E-mail
        # Parts of an email
        import smtplib
        from email.mime.text import MIMEText
        SERVER = app.config['MAIL_SERVER']
        PORT = app.config['MAIL_PORT']
        USER = app.config['MAIL_USERNAME']
        PASS = app.config['MAIL_PASSWORD']
        FROM = app.config['MAIL_DEFAULT_SENDER']
        TO = ['paulo.jorge.pm@gmail.com', 'paulo.jorge.pm@ilch.uminho.pt']
        SUBJECT = 'PAULO JORGE PM: NEWSLETTER MAIL'

        # Create the email
        message = MIMEText(date_now + "\r\nNOVO E-MAIL REGISTADO NA NEWSLETTER:\r\n" + email + "\r\nGOOD JOB PAULO :D")
        message['From'] = FROM
        message['To'] = ",".join(TO)
        message['Subject'] = SUBJECT

        # Sends an email
        email = smtplib.SMTP()
        email.connect(SERVER,PORT)
        email.starttls()
        email.login(USER,PASS)
        email.sendmail(FROM, TO, message.as_string())
        email.quit()
        return redirect( url_for('sucess'))
    else:
        if request.form.get('email'):
            mail = request.form.get('email').encode(encoding='UTF-8')

        return render_template("contact.html", right=right, mail=mail)

"""    if request.method == 'POST':
        #if from the footer form and needs to pass in recaptcha 1st
        if 'footer-email' in request.form:
            mail = request.form.get('footer-email').encode(encoding='UTF-8')
            return render_template("contact.html", right=right, mail=mail)
        #if main form in the contact page
        else:
            if recaptcha.verify():
                #Save in a log file
                email = request.form['email'].encode(encoding='UTF-8')
                date_now = datetime.now().strftime("%Y/%m/%d %H:%M:%S")
                txt_emails = os.path.join(os.path.dirname(__file__), 'logs', 'newsletter_email.txt')
                with open(txt_emails, "a") as txtfile:
                    txtfile.write(email + " - Data: " + date_now + "\r\n")
                #Send e-mail
                from flask_mail import Message
                msg = Message(body, recipients=[email])
                return redirect( url_for('sucess'))
            else:
                session["txt"]=""
                session["mail"]=request.form['email'].encode(encoding='UTF-8')
                return redirect( url_for('failed') )

    return render_template("contact.html", right=right, mail=mail)"""

@app.route("/playlist")
def playlist():
    right = right_index()
    playlist = Musics.query.filter( Musics.id > 0 ).order_by("day_start desc")
    return render_template("playlist.html", playlist=playlist, right=right)


def creat_photos_thumbnails(file=None):
    from PIL import Image
    import os

    SIZE = (500, 333)
    BASE_DIR = os.path.dirname(__file__)
    PHOTOS_DIR = os.path.join(BASE_DIR, "static","photos")
    THUMBNAILS_DIR = os.path.join(BASE_DIR, "static","photos_thumbnails")
    LOG = ""

    #if just one file
    if file:
        photos = []
        photos.append(file)
    #if all files
    else:
        photos = Photos.query.filter(Photos.id >= 0).order_by("id desc")

    for image in photos:
        photo_path = os.path.join(PHOTOS_DIR, image.path)
        LOG += "Origin path: " + photo_path
        #if thumbnail dont exist, create
        if not os.path.exists(os.path.join(THUMBNAILS_DIR, image.path.split(".")[0]+"_thumbnail."+image.path.split(".")[1])):
            try:
                img = Image.open(photo_path)
                img.convert('RGB')
                img.thumbnail(SIZE, Image.LANCZOS)
                save_path = os.path.join(THUMBNAILS_DIR, image.path.split(".")[0]+"_thumbnail."+image.path.split(".")[1])
                img.save(save_path, "JPEG", quality=100)
                LOG += " - <b><span style='color:green;'>SUCCESS</span></b> - " + "save path: " + save_path
            except:
                LOG += " - <b><span style='color:red;'>ERROR</span></b> - COULD NOT CREATE"
            LOG += "\n\r"
            # get file size in bytes
            photo_size = os.stat(save_path).st_size
        else:
            LOG += " - THUMBNAIL ALREADY EXISTED SAME NAME - <b><span style='color:blue;'>SKIPPED</span></b>"
            LOG += "\n\r"
    return LOG
    #return render_template("manage.html", log=LOG)
    #return self.render('manage.html')

@app.route("/photos_thumbnails")
def photos_thumbnails():
    right = right_index()
    try:
        log = creat_photos_thumbnails()
    except:
        log = "<b><span style='color:blue;text-decoration:underline;'>FAILED</span></b>"
    return render_template("photos_thumbnails.html", log=log, right=right)

#RUN IN THE FIRST TIME OF THE WEBSITE, TO CREAT THE DB'S
#Cuidado a partir daqui! Proteger com login ou desligar comentando
#Isto cria uma base de dados pela primeira vez e inicia o flask-migrate tirando uma espécie de fotografia do estrutura atual da db para fazer uma automática
#Para futuras migrations e mudanças na db, o init() não é necessário, apenas o migrate e o upgrade
@app.route("/setup")
def setup():
    #creat db's for first time
    #db.create_all()
    message = ""
    if not os.path.exists("migrations"):
        flask_migrate.init()
        message += "1: flask_migrate.init()<br>"
    else:
        message += "1: skip flask_migrate.init(), directory already exists<br>"
    flask_migrate.migrate()
    flask_migrate.upgrade()
    message += "2: flask_migrate.migrate()<br> 3: flask_migrate.upgrade()"
    return message

#Override some stuff in the main ModelView for every tab in Admin:
#id show in all tables + sort all in descending id order
#page size
#force auth login
#ect
class ModelView(ModelView):
    #display id
    column_display_pk = True
    #True for descending order
    column_default_sort = ('id',True)
    page_size = 20
    can_set_page_size = True
    can_export = True
    details_modal = True
    can_view_details = True
    def is_accessible(self):
        from werkzeug.exceptions import HTTPException
        from flask import Response
        auth = request.authorization or request.environ.get('REMOTE_USER')  # workaround for Apache
        if not auth or (auth.username, auth.password) != app.config['ADMIN_CREDENTIALS']:
            raise HTTPException('', Response(
                "Matrix limbo: the cake is a lie!", 401,
                {'WWW-Authenticate': 'Basic realm="Login Required"'}
            ))
        return True

#Flask-Admin
class PostsAdmin(ModelView):
    #column_list = ('id', 'title', 'body', 'tags', 'on_off', 'type', 'author', 'pub_date')
    #hide body and some other stuff, or table will be very big
    column_list = ('id', 'title', 'tags', 'on_off', 'type', 'pub_date')
    #list_columns
    """
    form_ajax_refs = {
        'tags': {
            'fields': (Tags.tag,)
        }
    }
    """
    def __init__(self, session):
        # Just call parent class with predefined model.
        super(PostsAdmin, self).__init__(Posts, session)

class PhotosAdmin(ModelView):
    column_list = ('id', 'title', 'album', 'tags', 'path', 'place', 'pub_date', 'on_off')

    # Override form field to use Flask-Admin FileUploadField
    form_overrides = {
        'path': form.FileUploadField,
    }

    # Pass additional parameters to 'path' to FileUploadField constructor
    form_args = {
        'path': {
            'label': 'Photos',
            'base_path': photos_path,
            'allow_overwrite': False
        }
    }

    form_choices = {
         'album': [
             ('geral', 'geral'),
             ('conceptual', 'conceptual'),
             ('personal', 'personal')
        ]
   }

    # After adding a new item, run the thumbnail creation
    def after_model_change(self, form, model, is_created):
            if is_created:
                creat_photos_thumbnails(file=model)

class MusicsAdmin(ModelView):
    # Override form field to use Flask-Admin FileUploadField
    form_overrides = {
        'path': form.FileUploadField
    }

    # Pass additional parameters to 'path' to FileUploadField constructor
    form_args = {
        'path': {
            'label': 'Musics',
            'base_path': musics_path,
            'allow_overwrite': False
        }
    }
    
class RascunhosAdmin(ModelView):
    column_list = ('id', 'body', 'type', 'pub_date')
    def __init__(self, session):
        super(RascunhosAdmin, self).__init__(Rascunhos, session)

class ManageView(BaseView):
    @expose('/')
    def index(self):
        log=""
        return self.render('manage.html', log=log)

admin = Admin(app, name='Borga', template_mode='bootstrap3', url='/editar', base_template="admin_template.html")
#admin.add_view(ModelView(Posts, db.session ))
admin.add_view(PostsAdmin(db.session))
admin.add_view(PhotosAdmin(Photos, db.session))
admin.add_view(ModelView(Tags, db.session))
admin.add_view(ModelView(PhotosTags, db.session))
#admin.add_view(ModelView(PersonalAlbums, db.session))
admin.add_view(MusicsAdmin(Musics, db.session))
admin.add_view(RascunhosAdmin(db.session))
admin.add_view(ManageView(name='Manage', endpoint='manage'))

#if __name__ == "__main__":
#    app.debug = True
#    app.run(host='0.0.0.0')
