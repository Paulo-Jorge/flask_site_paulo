import os
basedir = os.path.abspath(os.path.dirname(__file__))

DEBUG = False
SQLALCHEMY_TRACK_MODIFICATIONS = False
SQLALCHEMY_DATABASE_URI = 'sqlite:///' + os.path.join(basedir, 'databases/db.db')
#SQLALCHEMY_MIGRATE_REPO = os.path.join(basedir, 'migrate_db')
SQLALCHEMY_ECHO = True
SECRET_KEY = 'xxxxx'
#MAKO_OUTPUT_ENCODING = "UTF-8"
BASIC_AUTH_USERNAME = 'xxxxx'
BASIC_AUTH_PASSWORD = 'xxxxx'
ADMIN_CREDENTIALS = (BASIC_AUTH_USERNAME, BASIC_AUTH_PASSWORD)

MAIL_SERVER = 'xxxxx'
MAIL_PORT = 25
#MAIL_USE_TLS = False
#MAIL_USE_SSL = True
MAIL_USERNAME = "xxxxx"
MAIL_PASSWORD = "xxxxx"
MAIL_DEFAULT_SENDER = "xxxxx"
#MAIL_SUPPRESS_SEND = False
#MAIL_DEBUG = True
#MAIL_ASCII_ATTACHMENTS = default False
RECAPTCHA_ENABLED = True
RECAPTCHA_SITE_KEY = "xxxxx"
RECAPTCHA_SECRET_KEY = "xxxxx"
RECAPTCHA_THEME = "dark"# ou dark
RECAPTCHA_TYPE = "image"
RECAPTCHA_SIZE = "normal"
#RECAPTCHA_RTABINDEX = 10
